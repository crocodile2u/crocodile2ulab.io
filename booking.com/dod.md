---
layout: page
title: Definition of Done example
permalink: /dod/
---

# This is an example of what a Definition of Done might look like for a feature / user story

* Feature is implemented, which can be verified: via UI or otherwise
* Assumptions of User Story met
* Project builds without errors (might include static analysis checks)
* Unit / integration tests written and passing
* Project deployed on the test environment [nearly] identical to production platform
* Peer Code Review performed
* QA performed & issues resolved
* Feature is tested against acceptance criteria (specifically useful if BDD approach is used)
* Feature ok-ed by Product Owner
* Any configuration or build changes documented
* Documentation updated
