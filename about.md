---
layout: page
title: About me
manu: main
permalink: /about/
---

I am Victor Bolshov, web developer from Amsterdam, Netherlands. I was born in Russia in 1979, studied in Obninsk State University for Nuclear Power Engineering, as a physicist.

I started my programming journey in 2001, with PHP version 3. Since then, I learned programming languages, databases, technologies, concepts, systems. I try to continuously learn new things to stay up-to-date, and sometimes I share my findings. 
