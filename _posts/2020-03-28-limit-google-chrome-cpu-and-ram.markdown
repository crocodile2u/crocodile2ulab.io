---   
layout: post
title:  "Limit google chrome CPU and RAM"
date:   2020-03-28 08:59:22 +0100
categories: linux productivity
---
# Limit google chrome CPU and RAM

Today I was finally fed up with Google Chrome eating all available memory and occasionally causing the system to freeze. Ironically, I google solution using the same Google Chrome browser.

On my Ubuntu box I ended up with a `cgroups` solution found on [AskUbuntu](https://askubuntu.com/a/848022/36816) (answer by [sebastian-t](https://askubuntu.com/users/617717/sebastian-t)).

I think it's a perfect answer, it should be rated higher than the current top-rated one because that refers to older versions of Ubuntu and did not work out-of-the-box on my 20.04.

I tried to switch to Firefox because wanted to support that little diversity in browser engines that we are limited to these days... Unfortunately, it did not work out. FF was less stable and slower then Chrome (also Tridactyl extension clearly loses to Vimium, even with the latter not being able to work on some Google-owned sites). 

## UPD

I notice a lot of chrome tabs crashing all the time, because of restrictions. Well, so far I find it way better than a computer that occassionally stops responding to any action because of a memory leak...