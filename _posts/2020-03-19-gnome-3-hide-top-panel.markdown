---
layout: post
title:  "Hide top panel in Gnome 3"
date:   2020-03-19 22:59:22 +0100
categories: gnome
---
# Hide top panel in Gnome 3

I was a bit unsatisfied with annoying UX inconsistencies in Elementary OS (like Alt+Tab behavior or the keyboard switching lag), so I decided to move away from it and get back to Ubuntu. The new 20.04 Focal Fossa is close to be released so I didn't wait and took the risk.

The Gnome devs do not allow for too much customization of the top panel, you can't even set it to autohide, and I because find it absolutely redundant, I started looking for an extension that would hide it for me.

This one: [Hide Top Panel](https://extensions.gnome.org/extension/740/hide-top-panel/) is a good one that worked out-of-the-box. I think my desktop has become better, thank you [dimka665](https://extensions.gnome.org/accounts/profile/dimka665)!