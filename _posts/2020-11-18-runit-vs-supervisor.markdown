---   
layout: post
title:  "Runit vs Supervisor in Docker"
date:   2020-11-18 22:55:00 +0100
categories: docker
---

# Runit vs Supervisor in Docker

This is the contents of `README.md` in this Gitlab repository that I have created today: [runit-vs-supervisor](https://gitlab.com/crocodile2u/runit-vs-supervisor). Enjoy ;-)

Both [Runit](http://smarden.org/runit/) and [Supervisor](http://supervisord.org/) manage processes. They differ in philosophy and implementation, yet both are used quite often. When it comes to [Docker](https://docker.io), I personally have seen people using supervisor, but not runit, although it is IMO worth consideration. Supervisor is written in Python while Runit in C, and its memory and CPU footprint is reported to be negligible.

> Why would you need process supervision in Docker and thus violate the `service-per-container` principle? For instance, if you want to deploy your application on Google's `Cloud Run` serverless platform, maybe other similar environments.

In this project you will find Docker images based on [Alpine Linux](https://www.alpinelinux.org/) with runit & supervisor, running PHP-FPM & Nginx for a typical though old-school web application.

Running both images with docker, I was watching resource consumption by runit & supervisor processes, with htop. Turned out that supervisor consumed nearly 0% CPU and 0.5% memory on my laptop with `i7 (8 cores)` and 16GB RAM. Runit is a clear winner here with both metrics being nearly 0 throughout the observation.

Another important metric when it comes to Docker, is the image size. And again, just as I expected, Runit shines: `80MB` vs `126MB`.

In conclusion, I would like to encourage those who have a good usecase for process supervision in Docker, to try runit. It follows Unix philosophy of reliable simplicity and is highly configurable.