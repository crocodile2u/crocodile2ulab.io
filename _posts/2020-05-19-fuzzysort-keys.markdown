---   
layout: post
title:  "On fuzzysort keys"
date:   2020-05-19 08:10:00 +0100
categories: js fuzzysort
---

I recently discovered this awesomeness: [Fast SublimeText-like fuzzy search for JavaScript](https://github.com/farzher/fuzzysort). Super-small note: if you're wondering how to perform search on array of objects when the search subjects are deep in the object tree, it works just as you probably would expect:

```javascript
const rows = [{
    level1: {
        level2: "Hello fuzzy"
    }
}]
const withSingleKey = fuzzysort.go('hell', rows, {key: 'level1.level2'})
// or
const withMultipleKeys = fuzzysort.go('hell', rows, {keys: ['level1.level2']})
```

The `options` parameter has two ways of specifying key(s): `key` and `keys`. And you can provide a dot-separated path as the key name. 