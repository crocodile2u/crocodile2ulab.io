---   
layout: post
title:  "Move to custom domain bolshov.online"
date:   2020-04-22 15:10:00 +0100
categories: domain
---
# Move to custom domain

Since today, this blog is also available at [bolshov.online](https://bolshov.online/). Welcome everybody who's interested,
I'll try to keep you posted about tech (primarily), happenings in my life, journeys, music and movies.

For those curious: I bought domain name "bolshov.online" for only EUR1.30. It's not so crazy cheap to prolongate it though, ~EUR45. It's not too much either. I also got a free email address with a help from [connect.yandex.ru](https://connect.yandex.ru). Gitlab is super straightforward when it comes to configuring your custom domain, you just have to add a TXT record to your DNS zone file with a code they provide. Same goes for connect.yandex.ru, you add a TXT record to your DNS zone file to confirm that domain is actually yours.

Oh, and there was a small bug (or poor instructions thereof) when configuring gitlab pages. The instruction said I have to add a record like this:

```bash
_gitlab-pages-verification-code.bolshov.online     TXT     gitlab-pages-verification-code=XXXXXXXX
``` 

This didn't work, it only worked when I added this record:

```bash
bolshov.online     TXT     gitlab-pages-verification-code=XXXXXXXX
``` 

After that, everything worked like a charm, and I even don't have to worry about SSL certificate, it's all handled by [Gitlab](https://gitlab.com/) + [Letsencrypt](https://letsencrypt.org/).

Cheers, and see you on my new blog [bolshov.online](https://bolshov.online/)!