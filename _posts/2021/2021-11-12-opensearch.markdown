---   
layout: post
title:  "OpenSearch"
date:   2021-11-12 13:15:00 +0100
categories: web development
---

So, I started to look into how you would add a custom search engine with suggestions, to your browser.
Normally, you type in a search term in the browser location bar, and a dropdown would appear with suggestions from your preferred search engine.
In my case, that's DuckDuckGo:

![Default search engine suggestions](/images/opensearch/dd1.png)

I quickly discovered that there's a certain protocol that a website must implement so that your browser would allow you to **add it as a search engine**, like this:

![Search suggestions from php.net website](/images/opensearch/dd2.png)

In Firefox, you can add a new site as a search engine while you are browsing it. Later you can open [settings](about:preferences) and set a keyword for the new engine. I set the keyword for php.net search to "@php", now if I type @php in the location bar, Firefox switches to search with php.net and uses suggestions provided by php.net.

Now, the "certain protocol" I was talking about, is [OpenSearch](https://developer.mozilla.org/en-US/docs/Web/OpenSearch).
More information about autodiscovery (the feature I used to add php.net as a search engine), the search engine description format and suggestions protocol, you may find in [dewitt/opensearch](https://github.com/dewitt/opensearch) Github repo.

I also googled for PHP implementations of any tools related to OpenSearch but could only find a couple of libraries which don't provide too much functionality. 
Because I'm thinking of adding OpenSearch to my own side-project, I might implement a library of my own.
Will keep you posted!