---   
layout: post
title:  "OMG PHP, I thought I knew you!"
date:   2021-10-12 14:30:00 +0100
categories: development
---

After 20+ years working with PHP, I am still discovering useful and/or funny things about it!

1. You can insert an exclamation into the `DateTime::createFromFormat()` method call to reset all of the remaining time units.
    ```php
   // 2021-10-12 21:48:07.0
   DateTime::createFromFormat('Y-m-d', '2021-10-12');
   
   // 2021-10-12 00:00:00.0
   DateTime::createFromFormat('!Y-m-d', '2021-10-12');
    ```
   <p style="text-align: right">
      <a href="https://twitter.com/SteveTheBauman" target="_blank">@Steve Baumann</a> via
      <a href="https://twitter.com/SteveTheBauman/status/1448045021006082054" target="_blank">Twitter</a>
   </p>
2. You can increment ASCII strings with `++`
   ```php
   $x = 'a';
   $x++;// $x is now 'b'
   
   $x = 'ab';
   $x++;// ac
   
   $x = 'zz';
   $x++;// aaa
   
   $x = 'ы';// cyrillic 'y', unicode
   $x++;// ы (remains the same)
   ```
   I think this is a horrible feature, and a very inconsistent one. 
   Decrement doesn't work, and neither does `echo $x + 1;`, after `$x = '1'; $x++;` $x is no longer a string but int. 
   Overall it looks like a totally bad idea to rely on this feature. But, hail to BC, it's still here in **PHP 8**.