---   
layout: post
title:  "Starting soon at Booking.Com, Java & Perl"
date:   2021-10-15 13:30:00 +0100
categories: development
---

After 20+ years working with PHP, I am switching to Java & Perl as I am starting with my new job at [booking.com](https://booking.com/).

It's incredibly good that big players like Google, Amazon and Booking don't care about your particular background in dev, when considering your application and interviewing you. Learning a new programming language takes an experienced programmer a few months, during which they can already perform tasks and help the team.

This time, the process of finding a new job was particularly exhausting. I had interviews with a few smaller companies, then a whole rally at Amazon with a neutral feedback and no offer in the end, then a remarkably bad experience with a company called Gain.Pro in Amsterdam. Finally, a set of interviews with Booking engineers, and here I am, after 7 years since I first applied to a job vacancy there.

My family's been very supportive, my girlfriend cheered me up when I was saying I wasn't going to make it (and she brought me a nicest bottle of IPA ever, wrapped as a bunch of flowers in a local shop!).

Special thanks to [Vladimir Shefer](https://twitter.com/vladimirshefer) who conducted a fake job interview with me, for a position of a Java developer, which helped gain confidence. [Val Scherbak](https://twitter.com/Val_Scherbak), [Alexey Nakonechny](https://twitter.com/Allen_ru) - your support means a lot, as does that of each and every friend!