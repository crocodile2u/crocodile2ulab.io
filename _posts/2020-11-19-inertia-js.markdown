---   
layout: post
title:  "Inertia JS with Laravel"
date:   2020-11-19 14:22:00 +0100
categories: laravel js
---

# Inertia JS is another approach to Frontend with Laravel

Just a couple of links here, I forgot the name `inertia` and had to search the web for quite a while before I found it
again:

* [Inertia JS website](https://inertiajs.com/)
* [A comprehensive blog post about integrating Inertia with Laravel](https://deliciousbrains.com/inertiajs-building-single-page-apps/)

Cheers.