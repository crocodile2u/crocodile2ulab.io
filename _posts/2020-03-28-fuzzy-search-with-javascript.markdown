---   
layout: post
title:  "Fuzzy search with javascript"
date:   2020-03-28 22:59:22 +0100
categories: javascript search
---
# Fuzzy search with javascript

Compared two liraries for fuzzy search with JavaScript: [fuse.js](https://fusejs.io/) and [fuzzysort](https://github.com/farzher/fuzzysort). **Fuse** has an awesome website and **10K** stars on [Github](https://github.com/krisk/fuse), whereas **fuzzysort** is apparently only presented on Github where it has **2K** stars. Unlucky for **fuse.js**, this is where its advantages end for me. Quality of search  provided by **fussysort** is IMO _way_ better. **Fuse.js** has options that you can play around with, I tried setting threshold, using the query language - and every time the results were quite confusing, on their own demo page. **Fuzzysort**, on the other hand, produced very good result quality without having to fine-tune it. 