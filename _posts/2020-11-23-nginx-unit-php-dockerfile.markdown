---   
layout: post
title:  "Nginx Unit with PHP Dockerfile"
date:   2020-11-19 23:05:00 +0100
categories: nginx docker
---

# Extensible Dockerfile template with Nginx Unit and PHP

I found out that official PHP docker images don't have `embed` variant, nor do they compile embedded version of PHP
in any of existing variants. There's an [ongoing discussion](https://github.com/docker-library/php/pull/939) but
those who want the ebmedded version, have to find workarounds at the time of writing.

To address this issue, I have cloned the official [docker-library/php](https://github.com/docker-library/php) repo
and added `--enable-embed` to all the CLI `Dockerfiles`.

The result is [published on Dockerhub](https://hub.docker.com/repository/docker/crocodile2u/php-embed). 
However, I only pushed the `alpine3.12`/`php7.4` version. You can clone the dockerfiles repo above and build
whatever flavour you like.

In order to be able to have different base PHP images for different projects, with different extensions etc,
I have made a Dockerfile with multistage build:

```
FROM crocodile2u/php-embed AS php

######################################################################################
# add PHP extensions here, they'll be added to resulting image with their .ini files #
######################################################################################

FROM alpine AS unit

COPY --from=php /usr/lib /usr/lib
COPY --from=php /usr/local/bin/php-config /usr/local/bin/php-config
COPY --from=php /usr/local/include/php /usr/local/include/php
COPY --from=php /usr/local/lib/libphp7.so /usr/lib/libphp7.so
COPY --from=php /usr/local/etc/php /usr/local/etc/php

ENV UNIT_VERSION=1.21.0
RUN apk add --no-cache curl pcre-dev && apk add --virtual deps gcc musl-dev make \
     && curl -sLO https://unit.nginx.org/download/unit-${UNIT_VERSION}.tar.gz \
    && tar xzf unit-${UNIT_VERSION}.tar.gz && cd unit-${UNIT_VERSION} \
    && ./configure --prefix=/usr --state=/var/lib/unit --pid=/var/run/unit.pid \
        --log=/var/log/unit.log --tmp=/var/tmp --control=unix:/var/run/control.unit.sock \
    && ./configure php --module=php --config=/usr/local/bin/php-config \
    && make && make install && make clean \
    && cd .. && rm -rf unit-${UNIT_VERSION}* \
    && apk del deps

EXPOSE 80
WORKDIR /app

CMD ["unitd", "--no-daemon", "--log", "/dev/stdout"]
``` 

Now you can build you Unit/PHP image with this command:

```
docker build -t my-tag --target unit . 
```

I still have to add a bunch of usefull stuff that the official [Nginx Unit](https://unit.nginx.org/) dockerfiles have,
so that it supports configuration saved in `/docker-entrypoint.d/*.json` etc. Yet I think you got the idea.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Discuss Nginx+PHP in Docker<a href="https://t.co/9EI7A4Lph8">https://t.co/9EI7A4Lph8</a></p>&mdash; Victor Bolshov (@crocodile2u) <a href="https://twitter.com/crocodile2u/status/1330805673295306756?ref_src=twsrc%5Etfw">November 23, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 

