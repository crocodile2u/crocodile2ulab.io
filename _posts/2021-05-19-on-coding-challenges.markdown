---   
layout: post
title:  "On coding challenges"
date:   2021-05-19 11:45:00 +0100
categories: job interviews
---

# On coding challenges and test assignments as part of interviewing process

Dear recruiters, I have commited to the following in regards to my next job interviews:

I'm going to refuse any test assignments or coding challenges, because I myself as well as quite some of my colleagues don't find those a good practice for interviews. 

Test assignments always take much more than the claimed 1-2 hours, if you want to accomplish them honestly (most of the time you can easily google for a solution) and on a good level. You want to see my code - I make no secret of my composer packages and other Gitlab/Github repositories. 

Online coding challenges: despite 20+ years of experience, I can get nervous, and this is also true for a lot of other specialists.

Please take this into account. Cheers.