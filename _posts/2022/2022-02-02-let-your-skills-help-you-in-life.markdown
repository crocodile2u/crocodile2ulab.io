---
layout: post
title:  "Let your skills help you in life"
date:   2022-02-02 08:47:22 +0100
categories: linux shell
---

I have applied recently for an extension of my residence permit, it's going to be issued in mid March.
To get it, I have to go to the Immigration service desk and make a fresh photo.
I tried to register online but the nearest date was April, 4!
I tried to refresh the page in a few minutes and noticed that other dates appear from time to time.
I didn't want to waste time constantly monitoring the IND site, but I did want to get an appointment 
before I can get my documents, therefore I examined the network calls that the reservation page makes,
and wrote this tiny little script:

```shell
#!/bin/sh

earliest_available=$(curl -s "https://oap.ind.nl/oap/api/desks/AM/slots/?productKey=BIO&persons=1" | tail -1|jq -r '.data[] | {date} | join("")'| sort -u | head -1)
[ ! -z "$earliest_available" ] && expr "$earliest_available" \< "2022-04-01" && notify-send "IND BIO: $earliest_available is available!"
exit 0
```

What it does? It fetches JSON document containing available appointment slots, for Amsterdam IND desk (`desks/AM`),
then it strips the first line because IND website developers have some weird opinion on JSON.
Then it gets all the dates from all entries using `jq` utility which can query JSON.
Dates are sorted, then we take the top one, which is also going to be the earliest available.
In case that date is not empty and it is earlier then 1st of April, we send a desktop notification.

I saved it as `~/bin/ind-bio-alert.sh` and ran `chmod +x` on it (`~/bin` is in my `$PATH`, of coures). 
Then I ran it periodically, every 5 minutes, with `watch`:

```shell
watch -n 300 ind-bio-alert.sh
```

It had to run a few times before I got a notification, and registered myself for an appointment in the beginning of March!