---
layout: post
title:  "DNS issues after upgrading Ubuntu from 22.04 to 22.10"
date:   2022-10-21 18:00:22 +0100
categories: linux
---

# DNS issues after upgrading Ubuntu from 22.04 to 22.10

## TD;DR

Apparently, the upgrade process of systemd and the network stack is yet to be fixed. Namely, in the process of upgrade, certain configuration defaults were likely
to have changed. As a result, right in the middle of upgrade, I have lost network connectivity, which resulted in some packages not being updated
(I believe, those installed as snaps), and the whole process marked as incomplete. After that, the internet remained unreachable because domain names failed to
be resolved to IP addresses:

```
host google.com
;; communications error to 127.0.0.1#53: connection refused
```

Whatever I did, the error persisted, until I changed the NetworkManager.conf at `/etc/NetworkManager/NetworkManager.conf` by adding `dns=default` to the `main` section:

```
[main]
plugins=ifupdown,keyfile

dns=default

[..other sections..]
```

After that:

```
sudo systemctl restart NetworkManager
```

... and finally I had a working DNS.

What I think might have happened is that NetworkManager got updated and configured, at the same time 
systemd-resolved got removed but not yet replaced with a newer version, so local DNS server was not running.
And the NetworkManager new defaults already rely on that server. At least, after the upgrade failure, I noticed
that my system was lacking `systemd-resolved` and `resolvectl`.

When I added `dns=default` to NetworkManager config, it stopped relying on local DNS server and populated /etc/resolve.conf with the actual
DNS config provided with DHCP lease. But as soon I had internet, I could just finish installation and remove that line again,
thus getting back to Ubuntu defaults - and it works.

Hope it'll save someone some time once.
