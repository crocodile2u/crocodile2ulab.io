---   
layout: post
title:  "Nginx Unit"
date:   2020-11-19 14:30:00 +0100
categories: nginx
---

# [Nginx Unit](https://unit.nginx.org/) combines the well-known Nginx with application server 

Yes, Apache and it's `mod_{your language name here}` is back!

Only it's Nginx, it's improved, it is configurable without the need to restart, 
and it has integrated [language modules](https://unit.nginx.org/howto/modules/)!

I will hopefully investigate it further in a short while and keep you posted. 
This project was announced a few years ago, but at that time it lacked certain features
 (namely, the ability to provide your own language modules, apparently) so I forgot about it.
 Now it seems like it has improved a lot since then, and it may be a nice platform for those
 who want an all-in-one WebServer+AppServer package, which also can be used in Docker.
 
Stay tuned!

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">I&#39;ll be happy to discuss this blog post with you on Twitter<a href="https://t.co/BNJmuqArIc">https://t.co/BNJmuqArIc</a></p>&mdash; Victor Bolshov (@crocodile2u) <a href="https://twitter.com/crocodile2u/status/1329717370173202432?ref_src=twsrc%5Etfw">November 20, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 