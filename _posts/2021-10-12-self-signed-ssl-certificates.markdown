---   
layout: post
title:  "Self-signed SSL certificates"
date:   2021-10-12 14:30:00 +0100
categories: development
---

# Self-signed SSL certificates

This post is simply to save this solution for the future me:
[BenMorel / dev-certificates on Github](https://github.com/BenMorel/dev-certificates).

Works like a charm.