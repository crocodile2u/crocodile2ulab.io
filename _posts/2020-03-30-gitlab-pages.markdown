---   
layout: post
title:  "A word on Gitlab Pages"
date:   2020-03-30 07:02:00 +0100
categories: gitlab
---
# A word on Gitlab Pages

IMO, [Gitlab](https://gitlab.com) offers the greatest static site/blog platform, and the most convenient. It has templates, you can set up your Jekyll blog in no time, it publishes your stuff through a pipeline which tells you if you made a mistake in metadata. All automatic, zero hassle. Kudos to devs!