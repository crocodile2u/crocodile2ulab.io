---
layout: page
title: Bookmarks
menu: main
permalink: /bookmarks/
---

I'm going to keep a list of development-related web pages for future reference.

* [Fast SublimeText-like fuzzy search for JavaScript](https://github.com/farzher/fuzzysort)
    > Absolutely awesome search in pure JS.
* [The Myth of Schema-less](http://rustyrazorblade.com/post/2014/2014-07-09-the-myth-of-schemaless/)
    > people have become more and more convinced that “schema-less” is actually a feature to be proud of (or even exists). [...] At best, we should be using the description “provides little to no help in enforcing a schema” or “you’re on your own, good luck.”
