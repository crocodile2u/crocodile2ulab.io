---
layout: page
title: CV / Resume
menu: main
permalink: /cv/
---

### Software Developer

I always strive to get a better knowledge of the domain I'm working with,
to speak the same language as those who get to decide on product features.
Big fan of unit testing and automated testing in general.

### My key skills

* **PHP**: a PHP developer since 2001, try me;
* **MySQL**: working with MySQL also 2001, complex queries, optimization etc;
* **SQL / RDBMS**: mostly focused on MySQL, I have worked with PostgreSQL & SQLite, I can start with some other DB without a steep learning curve;
* **PHP Frameworks**:
  * Laravel - hands-on experience, including APIs and CI/CD pipeline for Laravel projects;
  * Zend Framework 1 - building complex business logic, optimizing a huge unit test suite;
  * Phalcon, SlimPHP - some knowledge, mostly because pet-projects;
* **CI/CD**: Gitlab pipelines, including complete setup;
* **Linux**: Linux user since ~2005, quite fluent with shell;
* **Docker**: good knowledge of containers, how to build and use them, some experience with orchestration like k8s or docker swarm, good command of docker-compose;
* **JavaScript**: good knowledge of JS, including reactive component-oriented frameworks such as VueJS or React.;
* **Golang**: ~1 year 6 months of professional fulltime development;
* **Git / Version control systems**: yes, of course.

### Participation in community

**[My composer packages](https://packagist.org/users/crocodile2u/packages/)**: check out [crocodile2u/imgproxy-php](https://packagist.org/packages/crocodile2u/imgproxy-php) and  [crocodile2u/chainy](https://packagist.org/packages/crocodile2u/chainy). 

**Publications**: 
* [PHPUnit worst practices](https://www.phparch.com/article/phpunit-worst-practices/)
* [Creating PHP Extensions With Zephir](https://www.phparch.com/magazine/2016-2/november/)
* [Laravel with docker-compose](https://medium.com/swlh/laravel-with-docker-compose-de2190569084)
* [My blog on NomadPHP](https://nomadphp.com/user/crocodile2u/blog)

### Languages
Russian, English, Dutch (basic)

<span style="page-break-after: always;"></span>

### Work experience

* **HarlemNext**. Software Engineer. August 2020 - Present. Development of the new version of a legacy billing / finance system, while keeping the existing functionality intact, which would allow for a steady and smooth transition from the old system to the new.

* **Ecochain**. Software Engineer. February 2019 - July 2020. Contribute to the new project **mobius**: Life Cycle Assessment tool which helps calculate environmental impact of a product "from gate to grave"

* **Nederlandsche Betaal & Wisselmaatschappij N.V**. Senior Software Engineer. July 2017 - January 2019. Financial B2B app (currency exchanges for businesses), backend developer. Helped migrate the project to a better DB infrastructure and later, to Google Cloud. 

* **Emesa Nederland BV**. Senior Software Developer. October 2015 - June 2017. Develop & improve company's web-based software as a core team member for their main project [vakantieveilingen.nl](https://vakantieveilingen.nl). PHP+MySQL+JS, serverside programming & optimization, user interfaces improvements.

* **Badoo**. PHP Developer. May 2011 - June 2015.
Improve user experience, build new UI, maintain existing code, writing unit tests, developing new applications/modules for [Badoo](https://badoo.com) dating network, with 400 mln users.

* **Rasprodaga.Ru**. Senior Software Engineer. 2008 - 2010.
Working on a retail advertising project. Seriously rewritten engine; added sphinx search; created deploy procedure; introduced VCS into development
process. Team leader (2 people reported directly).

* **ChronoPay**. PHP Developer. 2006 - 2008 (2 years)
Maintain company's internal software, develop billing application

* **RosBusinessConsulting**. Software Engineer. 2006 - 2006. Working on PDA versions of news sites, then on photofile.ru - one of the largest russian photo hosting projects at the time.

* **4th Rome**. Software Engineer. 2001-2006. Building web sites and CMS for the clients of the company.

### Contact information

Koog aan de Zaan, North Holland Province, Netherlands
 * tel: +3123328736
 * email: victor@bolshov.online
 * linked-in: www.linkedin.com/in/victorbolshov-8566124
 * website: https://bolshov.online
 * github: https://github.com/crocodile2u

### Education

Obninsk Institute for Nuclear Power Engineering
Master's degree, Materials Science · (1995 - 2001)

### Hobbies

Downhill skiing, table tennis, guitar.
